#!/bin/bash

stty -echo
rm -f openocd.tmp
echo "Starting OpenOCD Server..."
xterm_geo="-geometry 0x0+0+0"
root_dir=$(pwd)
xterm $xterm_geo -e "cd $root_dir; openocd -f jtag/openocd.cfg -l openocd.tmp" &
pid=$!
sleep 3
echo "Attempting to program device..."
xterm $xterm_geo -e "echo \"adapter_khz 1000;reset halt;flash erase_sector 0 0 last;at91sam3 gpnvm set 1;flash write_bank 0 $root_dir/src/build/gcc/bin/ethernetcard-flash.bin 0;reset;shutdown;exit\" | telnet localhost 4444; exit" &
wait $pid
success=$(grep -o 'wrote [0-9]* bytes from file' openocd.tmp)
bytes=$(echo $success | grep -o '[0-9]*')
if [ -z "$success" ]
then
    echo "Program unsuccessful.  Consider using 'make program_explicit' for error messages."
else
    echo "Program Successful! $bytes Bytes written."
fi
echo "Script Done."
rm -f openocd.tmp
stty echo
