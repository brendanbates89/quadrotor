/*
 * (c) 2013 Elecyr
 * 
 * App Initialization header file.
 * 
 * Author:     Brendan Bates <brendan.bates@elecyr.com>
 * Version:    0.1
 * Build Date: June 4th, 2013
 *   
 */

 #ifndef ELECYR_APP_INIT_H
 #define ELECYR_APP_INIT_H
 
#ifdef	__cplusplus
extern "C" {
#endif

#include "common.h"

/*
 * Function Definitions
 */
void app_init( void );
void app_init_clock(void);
void app_init_peripherals(void);

#ifdef	__cplusplus
}
#endif
#endif
