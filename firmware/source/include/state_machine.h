/* 
 * File:   state_machine.h
 * Author: root
 *
 * Created on July 24, 2013, 8:17 PM
 */

#ifndef STATE_MACHINE_H
#define	STATE_MACHINE_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#define SUBSTATE(substate_table) substate_table, (sizeof(substate_table)/sizeof(substate_transition_t))
#define STATE(state_fn_p) &(state_fn_p)
    

/*
 * State Definitions
 */
enum states
{
    STATE_SETUP,
    STATE_DHCP,
};

/*
 * Substate Definitions
 */
enum substates
{    
    // Generic Entry and Exit Substates
    SUBSTATE_NULL,
    SUBSTATE_ENTRY,
    SUBSTATE_EXIT,
            
    // DHCP Substates
    SUBSTATE_DHCP_CONFIGURE,
    SUBSTATE_DHCP_DISCOVER,
    SUBSTATE_DHCP_REQUEST,
    SUBSTATE_DHCP_LEASED,
    SUBSTATE_DHCP_REREQUEST,
    SUBSTATE_DHCP_RELEASE,
    SUBSTATE_DHCP_APPLY
};

/*
 * Responses
 */
enum state_responses
{
    // General Responses
    RESPONSE_OK,
    RESPONSE_FAIL,
    RESPONSE_STALL,
    RESPONSE_RESET,
    RESPONSE_EXIT,
    RESPONSE_INCREMENT,
    
    // DHCP Responses
    RESPONSE_DHCP_CONFIG
};

/*
 * Substate Transitions
 */
typedef struct substate_transition {
    uint8_t src_substate;
    uint8_t dest_substate;
    uint8_t substate_response;
} substate_transition_t;

/*
 * State Definition
 * 
 * The Substate table size 
 */
typedef struct state
{
    uint8_t state_id;
    uint8_t (*state_func)(uint8_t);
    substate_transition_t* substate_table;
    uint8_t substate_table_size;
} state_t;


/*
 * State Transitions
 */
typedef struct state_transition {
    state_t* src_state;
    state_t* dest_state;
    uint8_t state_response;
} state_transition_t;

void state_machine_init(void);
void state_machine(void);
void state_transition(uint8_t);

/*
 * State Data Structures
 */
extern state_t state_setup;
extern state_t state_advertising;
extern state_t state_connected;
extern state_t state_parsing;
extern state_t state_actions;
extern state_t state_rules;
extern state_t state_updating;

/*
 * State Transition Table
 */
extern state_transition_t state_transitions[];
extern const uint32_t num_state_transitions;

/*
 * State Counter.
 * 
 * This can be used to do multiple actions within a substate.
 * Every time a new substate is entered, the state counter is
 * reset.
 */
extern uint8_t state_counter;

/*
 * Timeout Actions for the various UDP/TCP states.
 */
void dhcp_timeout_action(void);

#ifdef	__cplusplus
}
#endif

#endif	/* STATE_MACHINE_H */

