/*
 * (c) 2013 Elecyr
 * 
 * Exception Table header file.
 * 
 * Author:     Brendan Bates <brendan.bates@elecyr.com>
 * Version:    0.1
 * Build Date: June 4th, 2013
 *   
 */

#ifndef ELECYR_EXCEPTIONS_H
#define ELECYR_EXCEPTIONS_H

#ifdef	__cplusplus
extern "C" {
#endif

/*----------------------------------------------------------------------------
 *        Types
 *----------------------------------------------------------------------------*/

/* Function prototype for exception table items (interrupt handler). */
typedef void( *IntFunc )( void );

/* HandlerNotUsed function. */
void HandlerNotUsed(void);

/* Default handler, for uninitialized Handlers. */
void DefaultHandler(void);

#ifdef	__cplusplus
}
#endif
#endif
