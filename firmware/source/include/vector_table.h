/*
 * (c) 2013 Elecyr
 * 
 * Vector Table header file.  Defines the extern app initialization
 * and and main functions.
 * 
 * Author:     Brendan Bates <brendan.bates@elecyr.com>
 * Version:    0.1
 * Build Date: June 4th, 2013
 *   
 */

#ifndef ELECYR_VECTOR_TABLE_H
#define ELECYR_VECTOR_TABLE_H

#ifdef	__cplusplus
extern "C" {
#endif
    
 #include <stdint.h>
 
/* Initialize segments */
extern uint32_t _sfixed;
extern uint32_t _efixed;
extern uint32_t _etext;
extern uint32_t _srelocate;
extern uint32_t _erelocate;
extern uint32_t _szero;
extern uint32_t _ezero;
extern uint32_t _sstack;
extern uint32_t _estack;

/* Required Function Delcarations */
void ResetException( void ) ;

/* libc Initialization */
void __libc_init_array(void);

#ifdef	__cplusplus
}
#endif
#endif