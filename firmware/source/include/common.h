/*
 * (c) 2013 Elecyr
 * 
 * Common header file, useful for placing commonly used defines and
 * global extern function declarations for application-wide use.
 *
 * Currently includes all necessary Atmel Software Framework files
 * for system and peripheral configuration.
 * 
 * Author:     Brendan Bates <brendan.bates@elecyr.com>
 * Version:    0.1
 * Build Date: June 4th, 2013
 *   
 */

#ifndef ELECYR_COMMON_H
#define ELECYR_COMMON_H

#ifdef	__cplusplus
extern "C" {
#endif

extern int main(void);
    
/* 
 * Clock details.  Update this number when changing the system
 * clock settings.
 */
#define CORE_FREQ 12000000
#define PLL_MULT 660
#define PLL_DIV 125
#define PROCESSOR_FREQ (CORE_FREQ / PLL_DIV) * PLL_MULT

#define PROC_WAIT(ms) timer_len=ms*10;timer_ticks=0;while(timer_ticks<100){}
    
/*
 * ASF Chip Definition.
 * Use the SAM3N1, even if we are using the *unsupported* SAM3N0 (Thanks, Atmel!)
 */
#define __SAM3S2A__

/*
 * Flag to tell the ASF to not use the CMSIS initialization.
 */
#define DONT_USE_CMSIS_INIT

/*
 * Various Flash controller wait states.  The write wait state takes
 * more cycles to complete than a read at 46.08MHz.
 */
#define FLASH_APP_WAIT_STATE   3
#define FLASH_WRITE_WAIT_STATE 6
    

/*
 * Include the ASF files.
 */
 #include "compiler.h"
 #include "adc/adc.h"
 #include "chipid/chipid.h"
 #include "pdc/pdc.h"
 #include "pio/pio.h"
 #include "pio/pio_handler.h"
 #include "pmc/pmc.h"
 #include "pwm/pwm.h"
 #include "rstc/rstc.h"
 #include "rtc/rtc.h"
 #include "rtt/rtt.h"
 #include "tc/tc.h"
 #include "spi/spi.h"
 #include "uart/uart.h"
 #include "usart/usart.h"
 #include "wdt/wdt.h"

#define define_pin(name, pin_name, controller, pin_periph, attribute) \
static const pin_t name = \
{ \
    .pio = controller, \
    .periph = pin_periph, \
    .pin = pin_name, \
    .attr = attribute \
}
    
#define config_pin(pin_def) pio_configure(pin_def.pio, pin_def.periph, pin_def.pin, pin_def.attr)
#define pin_set(pin_def) pio_set(pin_def.pio, pin_def.pin)
#define pin_clear(pin_def) pio_clear(pin_def.pio, pin_def.pin)
    
typedef struct pin
{
    Pio* pio;
    pio_type_t periph;
    uint32_t pin;
    uint32_t attr;
} pin_t;

#ifdef	__cplusplus
}
#endif
#endif
