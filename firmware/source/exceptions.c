/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2010, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

/**
 * \file
 * This file contains the default exception handlers.
 *
 * \note
 * The exception handler has weak aliases.
 * As they are weak aliases, any function with the same name will override
 * this definition.
 */

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

#include "common.h"
#include "exceptions.h"

/*----------------------------------------------------------------------------
 *        Exported functions
 *----------------------------------------------------------------------------*/

void HandlerNotUsed(void){ DefaultHandler(); }
void DefaultHandler(void){ return; }

WEAK void NMI_Handler(void){ DefaultHandler(); }
WEAK void HardFault_Handler(void){ DefaultHandler(); }
WEAK void MemManage_Handler(void){ DefaultHandler(); }
WEAK void BusFault_Handler(void){ DefaultHandler(); }
WEAK void UsageFault_Handler(void){ DefaultHandler(); }
WEAK void SVC_Handler(void){ DefaultHandler(); }
WEAK void DebugMon_Handler(void){ DefaultHandler(); }
WEAK void PendSV_Handler(void){ DefaultHandler(); }
WEAK void SysTick_Handler(void){ DefaultHandler(); }
WEAK void SUPC_Handler(void){ DefaultHandler(); }
WEAK void RSTC_Handler(void){ DefaultHandler(); }
WEAK void RTC_Handler(void){ DefaultHandler(); }
WEAK void RTT_Handler(void){ DefaultHandler(); }
WEAK void WDT_Handler(void){ DefaultHandler(); }
WEAK void PMC_Handler(void){ DefaultHandler(); }
WEAK void EFC_Handler(void){ DefaultHandler(); }
WEAK void UART0_Handler(void){ DefaultHandler(); }
WEAK void UART1_Handler(void){ DefaultHandler(); }
WEAK void PIOA_Handler(void){ DefaultHandler(); }
WEAK void PIOB_Handler(void){ DefaultHandler(); }
WEAK void USART0_Handler(void){ DefaultHandler(); }
WEAK void TWI0_Handler(void){ DefaultHandler(); }
WEAK void TWI1_Handler(void){ DefaultHandler(); }
WEAK void SPI_Handler(void){ DefaultHandler(); }
WEAK void TC0_Handler(void){ DefaultHandler(); }
WEAK void TC1_Handler(void){ DefaultHandler(); }
WEAK void TC2_Handler(void){ DefaultHandler(); }
WEAK void ADC_Handler(void){ DefaultHandler(); }
WEAK void PWM_Handler(void){ DefaultHandler(); }
WEAK void SSC_Handler(void){ DefaultHandler(); }
WEAK void CRCCU_Handler(void){ DefaultHandler(); }
WEAK void ACC_Handler(void){ DefaultHandler(); }
WEAK void UDP_Handler(void){ DefaultHandler(); }