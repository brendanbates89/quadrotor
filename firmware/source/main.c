#include "common.h"
#include "app_init.h"
#include "state_machine.h"

/*
 * Main function.  This function is located at the app_entry point, which the
 * bootloader will automatically launch after system initialization.
 * 
 * This function should not be moved since it is linked to the bootloader.  The
 * app_entry will ensure it always remains at the same address.
 */

int main( void )
{
    static uint32_t i = 0;
    
    /* Initialize the application specific devices. */
    app_init();
    
    /*
     * Run the state machine in an infinite loop.
     */
    while(1)
    {
        i++;
    }
    
    return 1;
}
