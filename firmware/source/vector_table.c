/*
 * (c) 2013 Elecyr
 * 
 * Vector Table Definition.  The exception_table is located at the
 * beginning of the program memory on the AT91SAM3.  Details on the
 * vector table can be found in the datasheet.
 * 
 * Author:     Brendan Bates <brendan.bates@elecyr.com>
 * Version:    0.1
 * Build Date: June 4th, 2013
 *   
 */

#include "common.h"
#include "exceptions.h"
#include "vector_table.h"

/* Exception Table */
__attribute__ ((section(".vectors")))
IntFunc exception_table[] = {

	/* Configure Initial Stack Pointer, using linker-generated symbols */
	(IntFunc) (&_estack),
	ResetException,

	NMI_Handler,
	HardFault_Handler,
	MemManage_Handler,
	BusFault_Handler,
	UsageFault_Handler,
	0, 0, 0, 0,        /* Reserved */
	SVC_Handler,
	DebugMon_Handler,
	0,                 /* Reserved  */
	PendSV_Handler,
	SysTick_Handler,

	/* Configurable interrupts  */
	SUPC_Handler,   /* 0  Supply Controller */
	RSTC_Handler,   /* 1  Reset Controller */
	RTC_Handler,    /* 2  Real Time Clock */
	RTT_Handler,    /* 3  Real Time Timer */
	WDT_Handler,    /* 4  Watchdog Timer */
	PMC_Handler,    /* 5  PMC */
	EFC_Handler,    /* 6  EFC */
	HandlerNotUsed,  /* 7  Reserved */
	UART0_Handler,  /* 8  UART0 */
	UART1_Handler,  /* 9  UART1 */
#ifdef ID_SMC
	SMC_Handler,    /* 10 SMC */
#else
	HandlerNotUsed,
#endif
	PIOA_Handler,   /* 11 Parallel IO Controller A */
	PIOB_Handler,   /* 12 Parallel IO Controller B */
#ifdef ID_PIOC
	PIOC_Handler,   /* 13 Parallel IO Controller C */
#else
	HandlerNotUsed,
#endif
	USART0_Handler, /* 14 USART 0 */
#ifdef ID_USART1
	USART1_Handler, /* 15 USART 1 */
#else
	HandlerNotUsed,
#endif
	HandlerNotUsed,  /* 16 Reserved */
	HandlerNotUsed,  /* 17 Reserved */
#ifdef ID_HSMCI
	HSMCI_Handler,  /* 18 HSMCI */
#else
	HandlerNotUsed,
#endif
	TWI0_Handler,   /* 19 TWI 0 */
	TWI1_Handler,   /* 20 TWI 1 */
	SPI_Handler,    /* 21 SPI */
	SSC_Handler,    /* 22 SSC */
	TC0_Handler,    /* 23 Timer Counter 0 */
	TC1_Handler,    /* 24 Timer Counter 1 */
	TC2_Handler,    /* 25 Timer Counter 2 */
#ifdef ID_TC3
	TC3_Handler,    /* 26 Timer Counter 3 */
#else
	HandlerNotUsed,
#endif
#ifdef ID_TC4
	TC4_Handler,    /* 27 Timer Counter 4 */
#else
	HandlerNotUsed,
#endif
#ifdef ID_TC5
	TC5_Handler,    /* 28 Timer Counter 5 */
#else
	HandlerNotUsed,
#endif
	ADC_Handler,    /* 29 ADC controller */
#ifdef ID_DACC
	DACC_Handler,   /* 30 DACC controller */
#else
	HandlerNotUsed,
#endif
	PWM_Handler,    /* 31 PWM */
	CRCCU_Handler,  /* 32 CRC Calculation Unit */
	ACC_Handler,    /* 33 Analog Comparator */
	UDP_Handler,    /* 34 USB Device Port */
	HandlerNotUsed   /* 35 not used */
};

void ResetException( void )
{
    uint32_t *pSrc, *pDest ;

    /* Initialize the relocate segment */
    pSrc = &_etext ;
    pDest = &_srelocate ;    

    if ( pSrc != pDest )
    {
        for ( ; pDest < &_erelocate ; )
        {
            *pDest++ = *pSrc++ ;
        }
    }

    /* Clear the zero segment */
    for ( pDest = &_szero ; pDest < &_ezero ; )
    {
        *pDest++ = 0;
    }

    /* Set the vector table base address */
    pSrc = (uint32_t *)&_sfixed;
    SCB->VTOR = ( (uint32_t)pSrc & SCB_VTOR_TBLOFF_Msk ) ;

    if ( ((uint32_t)pSrc >= IRAM_ADDR) && ((uint32_t)pSrc < IRAM_ADDR+IRAM_SIZE) )
    {
	    SCB->VTOR |= 1 << SCB_VTOR_TBLBASE_Pos ;
    }

    /* libc array initialization */
    __libc_init_array();

    /* Call into the bootloader. The bootloader is statically linked in the
     * linker script, and will call into the applications main code.
     */
    main();

    /* Infinite loop */
    while ( 1 ) ;
}
