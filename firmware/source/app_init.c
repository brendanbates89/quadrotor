/*
 * (c) 2013 Elecyr
 * 
 * Application Initialization routines.  The app_init() function
 * is automatically called on system reset.  Use this function to
 * set up system configuration, such as processor frequency and
 * peripheral configuration.
 * 
 * Author:     Brendan Bates <brendan.bates@elecyr.com>
 * Version:    0.1
 * Build Date: June 4th, 2013
 *   
 */

#include "common.h"
#include "app_init.h"
#include "state_machine.h"
#include "efc/efc.h"

/*
 * Called by the framework on system reset.
 */
void app_init( void )
{
    app_init_clock( );
    app_init_peripherals( );
}

/*
 * app_init_clock
 * 
 * Initializes the system clock.
 */
void app_init_clock(void)
{
    /*
     * Set the Flash Wait State to 3 cycles.  This allows the processor to run
     * at higher frequencies.
     */
    //EFC->EEFC_FMR = EEFC_FMR_FWS(2);
    efc_init(EFC, EFC_ACCESS_MODE_128, FLASH_APP_WAIT_STATE);
    

    // Configure Main Clock for 12MHz usage.
    PMC->CKGR_MOR = (CKGR_MOR_KEY(0x37)      // Key Value
                  | CKGR_MOR_MOSCRCEN        // RC Oscillator Enabled
                  | CKGR_MOR_MOSCRCF_12_MHz) // 12MHz RC Oscillator
                  & ~CKGR_MOR_MOSCSEL;       // Select RC Oscillator as the Main Oscillator

    /*    
     * Set the Oscillator Stabilization for 64 cycles (8 * 8) of the Slow Clock
     * to allow the main clock to stabilize.
     */
    PMC->CKGR_MOR |= CKGR_MOR_MOSCXTST(0x8);

    /*
     * Poll the MOSCRCS RC Oscillator Status register, wait for
     * stabilization.
     */
    while(!(PMC->PMC_SR & PMC_SR_MOSCRCS));

    /*
     * Initialize PLL to give 192/125 factor on the 12MHz frequency.
     * This gives a final frequency of 18.432MHz, to allow the UART
     * to have minimal error at 115200 baud.
     */
    PMC->CKGR_PLLAR = CKGR_PLLAR_ONE
                    | CKGR_PLLAR_MULA(PLL_MULT - 1)         // Multiply by 192 (MUL + 1)
                    | CKGR_PLLAR_PLLACOUNT(0x8)    // 64 Cycles (8*8) of Slow Clock
                    | CKGR_PLLAR_DIVA(PLL_DIV);        // Divide by 125

    // Wait for the PLL to stabilize.
    while(!(PMC->PMC_SR & PMC_SR_LOCKA));

    // Set no prescaler.
    PMC->PMC_MCKR &= ~PMC_MCKR_PRES_Msk;
    while(!(PMC->PMC_SR & PMC_SR_MCKRDY));

    // Set the PLL as the main/processor clock.
    PMC->PMC_MCKR = PMC_MCKR_CSS_PLLA_CLK;
    while(!(PMC->PMC_SR & PMC_SR_MCKRDY));
}

/*
 * app_init_peripherals
 * 
 * Initializes all of the SAM3N peripherals needed for the
 * application.
 */
void app_init_peripherals (void)
{
    // Disable the Watchdog Timer to stop resets.
    // Test commit.
    wdt_disable(WDT);

    // SysTick at about 3ms
    SysTick_Config(PROCESSOR_FREQ / 10000);
}
